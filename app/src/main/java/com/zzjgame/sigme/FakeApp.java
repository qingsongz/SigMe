package com.zzjgame.sigme;

import android.app.Application;
import android.content.Context;

import com.zzjgame.killer.SignEntry;

public class FakeApp extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        SignEntry.run();
        super.attachBaseContext(base);
    }
}
