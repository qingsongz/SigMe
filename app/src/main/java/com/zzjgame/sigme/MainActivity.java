package com.zzjgame.sigme;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zzjgame.sigme.util.Logger;
import com.zzjgame.sigme.util.PkgManager;
import com.zzjgame.sigme.view.IRecyclerData;
import com.zzjgame.sigme.view.ItemBean;
import com.zzjgame.sigme.view.RecyclerAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<ItemBean> allData;
    private PkgManager pkgManager;
    private TextView hintTxt;
    private String tmpPkg;
    private Button btnOk;

    private final IRecyclerData recyclerData = new IRecyclerData() {
        @Override
        public void receiveData(String txt, String pkg) {
            hintTxt.setText(txt);
            tmpPkg = pkg;
        }
    };

    private final View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!TextUtils.isEmpty(tmpPkg)){
                outputSig();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        Logger.set(true);
        initView();
        initData();
        startRecyclerView();
    }

    private void initView() {
        hintTxt = findViewById(R.id.hintText);
        btnOk = findViewById(R.id.okBtn);
    }

    private void initData() {
        hintTxt.setText("选择列表中的应用，获取应用的签名信息。");
        btnOk.setText("获取签名");
        btnOk.setOnClickListener(mClickListener);

        // 装载数据
        pkgManager = new PkgManager(this);
        allData = pkgManager.getAppList();
    }

    private void startRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerAdapter adapter = new RecyclerAdapter(this, allData);
        adapter.setDataDeal(recyclerData);
        recyclerView.setAdapter(adapter);
    }

    private void outputSig(){
        // 先获取sig
        String data = pkgManager.getApkSig(tmpPkg);

        // 然后UI提示
        String txt;
        if (data == null){
            txt = "无法获取应用签名\n" + tmpPkg;
            hintTxt.setText(txt);
            return;
        }
        txt = "签名获取成功，请查收。\n注意清理格式。";
        hintTxt.setText(txt);

        // 最后输出数据
        Logger.myLog("获取包名：" + tmpPkg);
        Logger.showMessage(data);
    }

}