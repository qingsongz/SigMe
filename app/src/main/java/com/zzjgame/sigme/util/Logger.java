package com.zzjgame.sigme.util;

import android.util.Log;

public class Logger {

    private static boolean bShow  = true;
    private static final String LOG = "zqsLog";

    public static void set(boolean open){
        bShow = open;
    }

    public static void myLog(String msg){
        if (bShow){
            Log.w(LOG, msg);
        }
    }

    public static void myError(String err){
        if (bShow){
            Log.e(LOG, err);
        }
    }

    public static void myException(Exception e){
        if (bShow){
            Log.e(LOG, e.toString());
        }
    }

   public static void showMessage(String str){
        int len = str.length();
        int nFileLength = 512 * 7;  // 3k大小，还有前面的一些其他的数据会占用一部分空间
        if (len > nFileLength){
            println(str);
        }else{
            myLog(str);
        }
    }

    private static void println(String longMsg){
        String buf;
        int start = 0, end, nSize = 3096, len = longMsg.length();
        while (start < len) {
            end = Math.min((start + nSize), len);
            buf = longMsg.substring(start, end);
            start += nSize;
            Log.d(LOG, buf);
        }
    }
}
