package com.zzjgame.sigme.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

public class Other {

    /**
     * 将Base编码的字符转为字符串(默认utf-8编码)
     * @param url Base字符
     */
    private static String base64String(String url) {
//        String url = "aHR0cHM6Ly9kb2NzLmdvb2dsZS5jb20vdWM/ZXhwb3J0PWRvd25sb2FkJmlkPTFyUHhFZVJXQzkxNmpoM01faGxnVU9FRWpfbUQzNWF4VA==";
        String result = null;
        try {
            result = new String(Base64.decode(url, 0), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Logger.myException(e);
        }
        return result;
    }

}
