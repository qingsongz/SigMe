package com.zzjgame.sigme.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import com.zzjgame.sigme.view.ItemBean;

import java.util.ArrayList;
import java.util.List;

public class PkgManager {

    private final PackageManager manager;
    private final ArrayList<ItemBean> datas = new ArrayList<>();

    public PkgManager(Context activity){
        manager = activity.getPackageManager();
    }

    public ArrayList<ItemBean> getAppList() {
        // 此处的 0 就是对返回的包信息没有特殊要求
        List<PackageInfo> packages = manager.getInstalledPackages(0);
        for (PackageInfo packageInfo : packages) {
            if (isThirdApp(packageInfo)) {
                ItemBean item = buildBean(packageInfo.packageName);
                if (item != null){
                    datas.add(item);
                }
            }
        }
        return datas;
    }

    public String getApkSig(String pkg){
        String result;
        try {
            PackageInfo info = manager.getPackageInfo(pkg, PackageManager.GET_SIGNATURES);
            byte[] signatures = info.signatures[0].toByteArray();
            StringBuilder builder = new StringBuilder();
            for (int i = 0, len = signatures.length - 1; i < len; ++i) {
                builder.append(signatures[i]);
                builder.append(",");
            }
            builder.append(signatures[signatures.length - 1]);
            result = builder.toString();
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    protected ItemBean buildBean(String apkPkg){
        String name = getAppName(apkPkg);
        if (name == null){
            return null;
        }
        Drawable icon = getIcon(apkPkg);
        return new ItemBean(name.trim(), apkPkg, icon);
    }

    private String getAppName(String packageName) {
        String result;
        try {
            ApplicationInfo info = manager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            result = manager.getApplicationLabel(info).toString();
        } catch (PackageManager.NameNotFoundException e) {
            result = null;
        }
        return result;
    }

    private Drawable getIcon(String pkg){
        Drawable result;
        try {
            result = manager.getApplicationIcon(pkg);
        } catch (PackageManager.NameNotFoundException e) {
            result = null;
        }
        return result;
    }

    private boolean isThirdApp(PackageInfo pi) {
        return (pi.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0;
    }
}
