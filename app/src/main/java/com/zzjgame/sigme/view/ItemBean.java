package com.zzjgame.sigme.view;

import android.graphics.drawable.Drawable;

public class ItemBean {

    private final String appName;
    private final String pkgName;
    private final Drawable appIcon;

    public ItemBean(String appName, String pkgName, Drawable appIcon) {
        this.appName = appName;
        this.pkgName = pkgName;
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public String getPkgName() {
        return pkgName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }
}
