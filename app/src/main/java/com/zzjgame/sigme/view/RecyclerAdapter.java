package com.zzjgame.sigme.view;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zzjgame.sigme.R;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private IRecyclerData mRecycler;
    private final ArrayList<ItemBean> mDatas;

    public RecyclerAdapter(Context context, ArrayList<ItemBean> datas){
        mContext = context;
        mDatas = datas;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_layout, parent, false);
        return new NormalHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NormalHolder normalHolder = (NormalHolder) holder;
        ItemBean itemBean = mDatas.get(position);
        normalHolder.appPkg.setText(itemBean.getPkgName());
        normalHolder.appName.setText(itemBean.getAppName());
        normalHolder.appIcon.setImageDrawable(itemBean.getAppIcon());

        setHolder(normalHolder, position);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void setDataDeal(IRecyclerData recyclerData){
        mRecycler = recyclerData;
    }

    private void setHolder(NormalHolder holder, int pos){
        // 设置斑马色
        if (pos % 2 == 0){
            holder.layout.setBackgroundColor(Color.parseColor("#AFEEEE"));
        }else{
            holder.layout.setBackgroundColor(Color.parseColor("#E0FFFF"));
        }

        // 设置点击事件
        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String msg = "已选择应用：\n" + holder.getAppName();
                String selectedPkg = String.valueOf(holder.getAppPkg());
                mRecycler.receiveData(msg, selectedPkg);
            }
        });
    }

    private static class NormalHolder extends RecyclerView.ViewHolder{

        private final LinearLayout layout;
        private final ImageView appIcon;
        private final TextView appName;
        private final TextView appPkg;

        public NormalHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.itemLayout);
            appIcon = itemView.findViewById(R.id.app_icon);
            appName = itemView.findViewById(R.id.app_name);
            appPkg = itemView.findViewById(R.id.app_pkg);
        }

        public CharSequence getAppName() {
            return appName.getText();
        }

        public CharSequence getAppPkg() {
            return appPkg.getText();
        }
    }
}
