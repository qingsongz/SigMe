package com.zzjgame.killer.inter;

public abstract class MethodInvocationProxy<T extends MethodInvocationStub> {

    private final T mInvocationStub;

    public MethodInvocationProxy(T invocationStub){
        mInvocationStub = invocationStub;
        onBindMethods();
        afterHookApply(invocationStub);
    }

    private void onBindMethods(){
    }
    private void afterHookApply(T delegate){
    }

    /**
     * 将需要代理的方法入库
     * @param methodProxy 待代理
     */
    public void addMethodProxy(MethodProxy methodProxy){
        mInvocationStub.addMethodProxy(methodProxy);
    }

    /**
     * 获取所有的要代理的类
     * @return 返回要代理的类
     */
    public T getInvocationStub(){
        return mInvocationStub;
    }
}
