package com.zzjgame.killer.inter;

import com.zzjgame.killer.util.ReflectException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class MethodProxy {

    private final boolean enable = true;

    public boolean beforeCall(Object who, Method method, Object... args){
        return true;
    }

    public Object call(Object who, Method method, Object... args) throws Throwable {
        try {
            return method.invoke(who, args);
        } catch (Exception e) {
            ReflectException.dealException(e);
        }
        return false;
    }

    public Object afterCall(Object who, Method method, Object[] args, Object result) throws Throwable {
        return result;
    }

    public boolean isEnable(){
        return enable;
    }

    public abstract String getMethodName();

    @Override
    public String toString() {
        return "MethodProxy{enable=" + enable + '}';
    }
}
