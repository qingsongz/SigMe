package com.zzjgame.killer.stub;

import android.content.pm.PackageInfo;
import android.os.IInterface;

import com.zzjgame.killer.inter.BinderInvocationStub;
import com.zzjgame.killer.inter.MethodInvocationProxy;
import com.zzjgame.killer.inter.MethodInvocationStub;
import com.zzjgame.killer.inter.MethodProxy;
import com.zzjgame.killer.util.ReflectException;
import com.zzjgame.killer.util.Reflection;
import com.zzjgame.killer.util.SignUtil;

import java.lang.reflect.Method;

public class PackageManagerStub extends MethodInvocationProxy<MethodInvocationStub<IInterface>> {

    private static final String SERVICE_PACKAGE = "package";

    public PackageManagerStub() {
        super(new MethodInvocationStub<>(getInterface()));
        init();
    }

    public static void replaceService() {
        PackageManagerStub serviceStub = new PackageManagerStub();
    }


    private void init() {
        addMethodProxy(new OwnPackageInfo());   // 将要代理的类先放好
        getBinder();                            // 获取对应的包服务

        try {
            // 将对应的服务给替换掉
            BinderInvocationStub pmHookBinder = new BinderInvocationStub(getInvocationStub().getBaseInterface());
            pmHookBinder.copyMethodProxies(getInvocationStub());
            pmHookBinder.replaceService(SERVICE_PACKAGE);
        } catch (Exception e) {
            ReflectException.dealException(e);
        }

        IInterface hookedPM;
        try {
            hookedPM = getInvocationStub().getProxyInterface();
            Object o = Reflection.on("android.app.ActivityThread").set("sPackageManager", hookedPM);
        } catch (Exception e) {
            ReflectException.dealException(e);
        }
    }

    private static IInterface getInterface() {
        Object service = Reflection.on("android.os.ServiceManager").call("getService", SERVICE_PACKAGE).getObject();
        return Reflection.on("android.content.pm.IPackageManager$Stub").call("asInterface", service).getObject();
    }

    private static void getBinder() {
        Reflection.on("android.os.ServiceManager").call("getService", SERVICE_PACKAGE).getObject();
    }


    /**
     * 改签名可以，改包名无效（一般不走这个包名）
     */
    private static class OwnPackageInfo extends MethodProxy {
        @Override
        public String getMethodName() {
            return "getPackageInfo";
        }

        @Override
        public Object call(Object who, Method method, Object... args) throws Throwable {
            PackageInfo result = (PackageInfo) method.invoke(who, args);
            if (result != null) {
                result.signatures = SignUtil.getSignature();
            }
            return result;
        }
    }
}
