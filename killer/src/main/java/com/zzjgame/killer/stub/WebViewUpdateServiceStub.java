package com.zzjgame.killer.stub;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.IInterface;

import com.zzjgame.killer.inter.BinderInvocationStub;
import com.zzjgame.killer.inter.MethodInvocationProxy;
import com.zzjgame.killer.inter.MethodInvocationStub;
import com.zzjgame.killer.inter.MethodProxy;
import com.zzjgame.killer.util.Reflection;
import com.zzjgame.killer.util.SignUtil;

import java.lang.reflect.Method;


public final class WebViewUpdateServiceStub extends MethodInvocationProxy<MethodInvocationStub<IInterface>> {

    private static final String SERVICE_WEBVIEWUPDATE = "webviewupdate";

    public WebViewUpdateServiceStub() {
        super(new MethodInvocationStub<>(getInterface()));
        init();
    }

    /**
     * 替换WebView服务
     */
    public static void replaceService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebViewUpdateServiceStub serviceStub = new WebViewUpdateServiceStub();
        }
    }

    private static IInterface getInterface() {
        Object service = Reflection.on("android.os.ServiceManager").
                call("getService", SERVICE_WEBVIEWUPDATE).getObject();

        return Reflection.on("android.webkit.IWebViewUpdateService$Stub").call("asInterface", service).getObject();
    }

    private static void getBinder() {
        Reflection.on("android.os.ServiceManager").
                call("getService", SERVICE_WEBVIEWUPDATE).getObject();
    }

    private void init() {
        addMethodProxy(new WaitForAndGetProvider());

        getBinder();
        BinderInvocationStub pmHookBinder = new BinderInvocationStub(getInvocationStub().getBaseInterface());
        pmHookBinder.copyMethodProxies(getInvocationStub());
        pmHookBinder.replaceService(SERVICE_WEBVIEWUPDATE);
    }

    private static class WaitForAndGetProvider extends MethodProxy {
        @Override
        public String getMethodName() {
            return "waitForAndGetProvider";
        }

        @Override
        public Object call(Object who, Method method, Object... args) throws Throwable {
            Object result = method.invoke(who, args);
            if (result != null) {
                PackageInfo inf = Reflection.on(result).get("packageInfo");
                if (inf != null) {
                    Reflection.on(inf).set("signatures", SignUtil.getSignature());
                }
            }
            return result;
        }
    }
}
