package com.zzjgame.killer.util;

import android.util.Log;

public final class ReflectException extends RuntimeException{

    public ReflectException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReflectException(Throwable cause) {
        super(cause);
    }

    public static void dealException(Exception e){
        Log.e("zyLog", e.toString());
    }
}
