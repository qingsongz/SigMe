package com.zzjgame.killer.util;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 维护单个的具体反射对象
 * 1、包括真实的对象实例
 * 2、表明其是否为类
 */
public final class Reflection {

    private final Object object;
    private final boolean bClazz;

    private Reflection(Class<?> type){
        object = type;
        bClazz = true;
    }

    private Reflection(Object obj){
        object = obj;
        bClazz = false;
    }

    public static Reflection on(String name){
        return on(forName(name));
    }

//    public static Reflection on(String name, ClassLoader classLoader){
//        return on(forName(name, classLoader));
//    }

    public static Reflection on(Class<?> clazz){
        return new Reflection(clazz);
    }

    public static Reflection on(Object obj){
        return new Reflection(obj);
    }

    // 工具方法

    private static <T extends AccessibleObject> T accessible(T t){
        if (t == null){
            return null;
        }
        if (t instanceof Member){
            Member member = (Member) t;
            if (Modifier.isPublic(member.getModifiers()) &&
                Modifier.isPublic(member.getDeclaringClass().getModifiers())){
                return t;
            }
        }

        if (!t.isAccessible()){
            t.setAccessible(true);
        }
        return t;
    }

    // 将基本数据类型转为对象类
    private static Class<?> wrapper(Class<?> type){
        if (type == null){
            return null;
        }
        if (!type.isPrimitive()) {
            return type;
        }

        Class<?> result = null;
        if (boolean.class == type) {
            result = Boolean.class;
        } else if (int.class == type) {
            result = Integer.class;
        } else if (long.class == type) {
            result = Long.class;
        } else if (short.class == type) {
            result = Short.class;
        } else if (byte.class == type) {
            result = Byte.class;
        } else if (double.class == type) {
            result = Double.class;
        } else if (float.class == type) {
            result = Float.class;
        } else if (char.class == type) {
            result = Character.class;
        } else if (void.class == type) {
            result = Void.class;
        }
        return result;
    }

    // 私有




    // 将构造方法转为实际对象
    private static Reflection on(Constructor<?> constructor, Object... args){
        Reflection result = null;
        try {
            result = on(accessible(constructor).newInstance(args));
        } catch (Exception e) {
            ReflectException.dealException(e);
        }
        return result;
    }

    /**
     * 直接让对象执行某个方法
     * @param method 方法
     * @param object 对象
     * @param args   参数
     * @return 实例
     */
    private static Reflection on(Method method, Object object, Object... args){
        accessible(method);
        try{
            if (method.getReturnType() == void.class){
                method.invoke(object, args);
                return on(object);
            }else {
                return on(method.invoke(object, args));
            }
        }catch (Throwable ignore){
        }
        return null;
    }

    /**
     * 获取真实实例
     * @param object 反射对象
     * @return 真实实例
     */
    private static Object unwrap(Object object){
        if (object instanceof Reflection){
            return ((Reflection)object).getObject();
        }
        return object;
    }

    /**
     * 将可变的参数变为类数组
     * @param values 可变长参数
     * @return 类数组
     */
    private static Class<?>[] types(Object... values){
        if (values == null){
            return new Class[0];
        }
        Class<?>[] result = new Class[values.length];
        for (int i = 0, len = values.length; i < len; ++i){
            Object value = values[i];
            result[i] = (value == null ? NULL.class : value.getClass());
        }
        return result;
    }

    private static Class<?> forName(String name){
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            ReflectException.dealException(e);
        }
        return null;
    }
//    private static Class<?> forName(String name, ClassLoader classLoader){
//        try {
//            return Class.forName(name,true, classLoader);
//        } catch (ClassNotFoundException e) {
//            ReflectException.dealException(e);
//        }
//        return null;
//    }

    /**
     * 获取反射对象里的真实对象
     * @param <T> 泛型
     * @return 真实对象
     */
    public <T> T getObject(){
        return (T) object;
    }

    /**
     * 获取指定变量的真实数据
     * @param name 变量名
     * @param <T> 泛型
     * @return 真实变量
     */
    public <T>T get(String name){
        return Objects.requireNonNull(field(name)).getObject();
    }


    /**
     * 将指定的变量更改为新的数据
     * @param name  变量名
     * @param value 新数据
     * @return  反射对象
     */
    public Reflection set(String name, Object value){
        try{
            Field field = turn2Field(name);
            field.setAccessible(true);
            field.set(object, unwrap(value));
            return this;
        }catch (Throwable ignore){
        }
        return null;
    }

    /**
     * 将指定的变量封装成一个新的反射对象
     * @param name 变量名
     * @return  反射对象
     * @throws ReflectException 异常
     */
    public Reflection field(String name) throws ReflectException{
        try{
            Field field = turn2Field(name);
            return on(field.get(object));
        }catch (Throwable ignore){
        }
        return null;
    }

    /**
     * 获取指定名称的变量
     * @param name 变量名
     * @return 变量字段
     */
    private Field turn2Field(String name) {
        Field result = null;

        Class<?> clazz = type();
        // 先尝试取得公有字段
        try {
            result = clazz.getField(name);
        } catch (NoSuchFieldException e) {
            // 此时尝试非公有字段
            do {
                try {
                    result = accessible(clazz.getDeclaredField(name));
                } catch (NoSuchFieldException ignore) {
                }
                clazz = clazz.getSuperclass();
            } while (clazz != null);
        }
        return result;
    }

    /**
     * 将字符串的首字母小写
     * @param string 字符串
     * @return 小写后的字符串
     */
    private static String property(String string) {
        int length = string.length();
        if (length == 0) {
            return "";
        } else if (length == 1) {
            return string.toLowerCase();
        } else {
            return string.substring(0, 1).toLowerCase() + string.substring(1);
        }
    }

// -----------------------------------

    /**
     * 调用方法根据传入的参数
     *
     * @param methodName 方法名
     * @param args       参数列表
     * @return           反射对象
     * @throws ReflectException 异常
     */
    public Reflection call(String methodName, Object... args) throws ReflectException {
        Class<?>[] types = types(args);
        try {
            Method method = exactMethod(methodName, types);
            return on(method, object, args);
        } catch (NoSuchMethodException e) {
            try {
                Method method = similarMethod(methodName, types);
                return on(method, object, args);
            } catch (NoSuchMethodException e1) {
                throw new ReflectException(e1);
            }
        }
    }

    /**
     * 根据名称和参数获取对应的方法
     * @param name  方法名
     * @param types 参数列表
     * @return      方法
     * @throws NoSuchMethodException 异常
     */
    private Method exactMethod(String name, Class<?>[] types) throws NoSuchMethodException {
        Class<?> type = type();
        try {
            return type.getMethod(name, types);
        } catch (NoSuchMethodException e) {
            do {
                try {
                    return type.getDeclaredMethod(name, types);
                } catch (NoSuchMethodException ignore) {
                }
                type = type.getSuperclass();
            } while (type != null);

            // 失败后直接抛异常
            throw e;
        }
    }

    /**
     * 根据参数和名称匹配方法
     */
    private Method similarMethod(String name, Class<?>[] types) throws NoSuchMethodException {
        Class<?> type = type();

        for (Method method : type.getMethods()) {
            if (isSimilarSignature(method, name, types)) {
                return method;
            }
        }

        do {
            for (Method method : type.getDeclaredMethods()) {
                if (isSimilarSignature(method, name, types)) {
                    return method;
                }
            }
            type = type.getSuperclass();
        } while (type != null);

        // 找不到对应的方法
        throw new NoSuchMethodException("No similar method " + name + " with params " + Arrays.toString(types)
                + " could be found on type " + type() + ".");
    }

    /**
     * 是否是想要的方法
     * @param matchingMethod        用来匹配的方法
     * @param desiredMethodName     想要的方法名
     * @param desiredParamTypes     想要的参数列表
     * @return  true，是想要的方法
     */
    private boolean isSimilarSignature(Method matchingMethod,
                                       String desiredMethodName, Class<?>[] desiredParamTypes) {
        return matchingMethod.getName().equals(desiredMethodName)
                && match(matchingMethod.getParameterTypes(), desiredParamTypes);
    }

    /**
     * 创建一个实例通过默认构造器
     *
     * @return 反射对象
     * @throws ReflectException 异常
     */
    private Reflection create() throws ReflectException {
        return create(new Object[0]);
    }

    /**
     * 创建一个实例根据传入的参数
     *
     * @param args 参数
     * @return 反射对象
     * @throws ReflectException 自定义异常
     */
    public Reflection create(Object... args) throws ReflectException {
        Class<?>[] types = types(args);
        try {
            Constructor<?> constructor = type().getDeclaredConstructor(types);
            return on(constructor, args);
        } catch (NoSuchMethodException e) {
            for (Constructor<?> constructor : type().getDeclaredConstructors()) {
                if (match(constructor.getParameterTypes(), types)) {
                    return on(constructor, args);
                }
            }

            throw new ReflectException(e);
        }
    }

    /**
     * 检查两个数组的类型是否匹配
     */
    private boolean match(Class<?>[] declaredTypes, Class<?>[] actualTypes) {
        if (declaredTypes.length != actualTypes.length) {
            return false;
        }
        for (int i = 0, len = actualTypes.length; i < len; i++) {
            if (actualTypes[i] == NULL.class)
                continue;
            // 如果数组中包含原始类型,将它们转换为对应的包装类型
            if (wrapper(declaredTypes[i]).isAssignableFrom(wrapper(actualTypes[i])))
                continue;

            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return object.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj) {
        return obj instanceof Reflection && object.equals(((Reflection) obj).getObject());
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return object.toString();
    }

    /**
     * 取得反射对象的类型
     *
     * @see Object#getClass()
     */
    private Class<?> type() {
        if (bClazz) {
            return (Class<?>) object;
        } else {
            return object.getClass();
        }
    }

    // ---- 未使用 ----

    public Map<String, Reflection> fields() {
        Map<String, Reflection> result = new LinkedHashMap<String, Reflection>();
        Class<?> type = type();

        do {
            for (Field field : type.getDeclaredFields()) {
                if (!bClazz ^ Modifier.isStatic(field.getModifiers())) {
                    String name = field.getName();

                    if (!result.containsKey(name))
                        result.put(name, field(name));
                }
            }

            type = type.getSuperclass();
        } while (type != null);

        return result;
    }

    public Reflection call(String name) throws ReflectException {
        return call(name, new Object[0]);
    }

    /**
     * 智能调用 但是只调用类本身声明方法 按照优先级 匹配
     * <p>
     * 1.完全匹配
     * 2.形参 Object...
     * 3.名字相同 无参数
     *
     * @param name
     * @param args
     * @return
     * @throws ReflectException
     */
    public Reflection callBest(String name, Object... args) throws ReflectException {
        Class<?>[] types = types(args);
        Class<?> type = type();

        Method bestMethod = null;
        int level = 0;
        for (Method method : type.getDeclaredMethods()) {
            if (isSimilarSignature(method, name, types)) {
                bestMethod = method;
                level = 2;
                break;
            }
            if (matchObjectMethod(method, name, types)) {
                bestMethod = method;
                level = 1;
                continue;
            }
            if (method.getName().equals(name) && method.getParameterTypes().length == 0 && level == 0) {
                bestMethod = method;
            }
        }
        if (bestMethod != null) {
            if (level == 0) {
                args = new Object[0];
            }
            if (level == 1) {
                Object[] args2 = {args};
                args = args2;
            }
            return on(bestMethod, object, args);
        } else {
            throw new ReflectException("no method found for " + name, new NoSuchMethodException("No best method " + name + " with params " + Arrays.toString(types)
                    + " could be found on type " + type() + "."));
        }
    }

    private boolean matchObjectMethod(Method possiblyMatchingMethod, String desiredMethodName,
                                      Class<?>[] desiredParamTypes) {
        return possiblyMatchingMethod.getName().equals(desiredMethodName)
                && matchObject(possiblyMatchingMethod.getParameterTypes());
    }

    private boolean matchObject(Class<?>[] parameterTypes) {
        Class<Object[]> c = Object[].class;
        return parameterTypes.length > 0 && parameterTypes[0].isAssignableFrom(c);
    }

    /**
     * 创建一个动态代理根据传入的类型. 如果我们正在维护的是一个Map,那么当调用出现异常时我们将从Map中取值.
     *
     * @param proxyType 需要动态代理的类型
     * @return 动态代理生成的对象
     */
    @SuppressWarnings("unchecked")
    public <P> P as(Class<P> proxyType) {
        final boolean isMap = (object instanceof Map);
        final InvocationHandler handler = new InvocationHandler() {

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String name = method.getName();
                try {
                    return on(object).call(name, args).getObject();
                } catch (ReflectException e) {
                    if (isMap) {
                        Map<String, Object> map = (Map<String, Object>) object;
                        int length = (args == null ? 0 : args.length);

                        if (length == 0 && name.startsWith("get")) {
                            return map.get(property(name.substring(3)));
                        } else if (length == 0 && name.startsWith("is")) {
                            return map.get(property(name.substring(2)));
                        } else if (length == 1 && name.startsWith("set")) {
                            map.put(property(name.substring(3)), args[0]);
                            return null;
                        }
                    }

                    throw e;
                }
            }
        };

        return (P) Proxy.newProxyInstance(proxyType.getClassLoader(), new Class[]{proxyType}, handler);
    }


    public static String getMethodDetails(Method method) {
        StringBuilder sb = new StringBuilder(40);
        sb.append(Modifier.toString(method.getModifiers()))
                .append(" ")
                .append(method.getReturnType().getName())
                .append(" ")
                .append(method.getName())
                .append("(");
        Class<?>[] parameters = method.getParameterTypes();
        for (Class<?> parameter : parameters) {
            sb.append(parameter.getName()).append(", ");
        }
        if (parameters.length > 0) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(")");
        return sb.toString();
    }

    private static String propery(String name){
        int len = name.length();
        if (len == 0){
            return "";
        }
        if (len == 1){
            return name.toLowerCase();
        }else{
            return name.substring(0,1).toLowerCase() + name.substring(1);
        }
    }

    /**
     * 空类
     */
    private static class NULL{
        // 单纯用来表征的，无实际意义
    }

}
