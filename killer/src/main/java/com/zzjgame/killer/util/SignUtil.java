package com.zzjgame.killer.util;

import android.content.pm.Signature;

import com.zzjgame.killer.SignEntry;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public final class SignUtil {

    private static Signature[] sSignature = null;
    private static final String mPre = "zqsLog";

    /**
     * 获取游戏的原始签名信息
     * @return 原始签名
     */
    public static Signature[] getSignature(){
        if (sSignature != null){
            return sSignature;
        }
        sSignature = new Signature[1];
        sSignature[0] = new Signature(getSigBytes());
        return sSignature;
    }

    private static byte[] getSigBytes(){
        String array = SignEntry.getSig();
        String strPre = "D/" + mPre + ": ";
        array = array.replace(strPre, "").replace("\n", "");

        String[] arrStr = array.split(",");
        int len = arrStr.length;
        byte[] buff = new byte[len];
        for (int i = 0; i < len; ++i){
            buff[i] = Byte.parseByte(arrStr[i]);
        }
        return buff;
    }

    /**
     * 获取指定类里面的所有的接口（包括父类）
     * @param clazz 指定类
     * @return 所有的接口
     */
    public static Class<?>[] getAllInterface(Class clazz){
        HashSet<Class<?>> classHashSet = new HashSet<>();
        getAllInterfaces(clazz, classHashSet);

        // 将结果转换位数组
        Class<?>[] result = new Class[classHashSet.size()];
        classHashSet.toArray(result);
        return result;
    }

    /**
     * 将对应的接口保存到集合中（递归）
     * @param clazz 指定类
     * @param interfaceCollection 接口集合
     */
    private static void getAllInterfaces(Class clazz, HashSet<Class<?>> interfaceCollection){
        Class<?>[] clazzs = clazz.getInterfaces();
        if (clazzs.length != 0){
            interfaceCollection.addAll(Arrays.asList(clazzs));
        }
        if (clazz.getSuperclass() == Object.class){
            return;
        }

        try{
            Class obj = Objects.requireNonNull(clazz.getSuperclass());
            getAllInterfaces(obj, interfaceCollection);
        }catch (Exception ignored){
        }
    }
}
